package com.allstate.javaassessment.javaassessmentspring.controllers;

import com.allstate.javaassessment.javaassessmentspring.dto.Payment;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PaymentControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;
    @Test
    public void testStatus(){
        ResponseEntity<String> responseEntity = restTemplate.exchange("/api/payment/status", HttpMethod.GET, null, new ParameterizedTypeReference<String>() {
        });
        // restTemplate.postForEntity()
        String responseBody = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseBody.contains("calling"));
    }
    @Test
    public void testSave(){
        Date date = new Date();
        Payment payment = new Payment(4,date,"paytm",150,999);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("/api/payment/save",payment,String.class);
        assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void testRowCount(){
        ResponseEntity<String> responseEntity = restTemplate.exchange("/api/payment/getrowcount", HttpMethod.GET, null, new ParameterizedTypeReference<String>() {
        });
        String responseBody = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());


    }
    @Test
    public void testFindById(){
        Map<String, String> params = new HashMap<String, String>();
        params.put("id", "2");
        Payment payment = restTemplate.getForObject("/api/payment/findbyid/{id}",Payment.class,params);
        assertEquals(2,payment.getId());
    }

    @Test
    public void testFindByType(){
        Map<String, String> params = new HashMap<String, String>();
        params.put("type", "card");

        ResponseEntity<List> response =restTemplate.getForEntity("/api/payment/findbytype/{type}",List.class,params);
        List<Payment> payments= response.getBody();
        assertTrue(payments.size()>0);
    }
    @Test
    public void testFindAll(){
        ResponseEntity<List> response =restTemplate.getForEntity("/api/payment/find",List.class);
        List<Payment> payments= response.getBody();
        assertTrue(payments.size()>0);
    }

}
