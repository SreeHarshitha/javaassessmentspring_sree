package com.allstate.javaassessment.javaassessmentspring.dto;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PaymentTest {
   @Test
    void paymentTest(){
        Date date = new Date();//LocalDate.parse("2021-04-09");
        Payment payment = new Payment(1, date,"cash",100,123);
          assertEquals(1,payment.getId());
          assertEquals(date,payment.getPaymentdate());
          assertEquals("cash",payment.getType());
          assertEquals(100,payment.getAmount());
          assertEquals(123,payment.getCustid());
    }
    @Test
    void toStringTest(){

        Date date = new Date();

        Payment payment = new Payment();
        payment.setId(1);
        payment.setPaymentdate(date);
        payment.setAmount(200);
        payment.setType("card");
        payment.setCustid(456);
        assertEquals("Payment{" +"id=" + payment.getId() +", paymentdate=" + payment.getPaymentdate() +", type='" +
                "" + payment.getType() + '\'' +", amount=" + payment.getAmount()+", custid=" + payment.getCustid() +'}',payment.toString());

    }


}
