package com.allstate.javaassessment.javaassessmentspring.dao;

import com.allstate.javaassessment.javaassessmentspring.dto.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PaymentRepoTest {

    @Autowired
    PaymentRepo paymentRepo;

    @Test
    public void test_rowcount(){
        System.out.println("no of records :"+paymentRepo.count());
        assertTrue(paymentRepo.count()>=0);


    }

    @Test
    public void test_findByIdOptionalEmpty(){
        assertEquals(Optional.empty(),paymentRepo.findById(89));
    }

    @Test
    public void test_findByIdOptionalNotEmpty(){
        assertNotEquals(Optional.empty(),paymentRepo.findById(2));
    }

    @Test
    public void test_saveCustomer(){
        Date date = new Date();
        Payment payment = new Payment(2,date,"card",200,444);
        paymentRepo.save(payment);
        assertNotEquals(Optional.empty(),paymentRepo.findById(2));

    }

    @Test
    public void test_findall(){
        assertNotEquals(Optional.empty(),paymentRepo.findAll());
    }
    @Test
    public void test_findByType(){
       List<Payment> paymentList = new ArrayList<>();
        paymentList = paymentRepo.findByType("card");
        Iterator iter = paymentList.listIterator();
        while(iter.hasNext()){
            Payment payment = (Payment) iter.next();
            System.out.println(payment.getPaymentdate());
        }
        assertNotNull(paymentRepo.findByType("card"));
    }

}
