package com.allstate.javaassessment.javaassessmentspring.services;

import com.allstate.javaassessment.javaassessmentspring.exceptions.OutOfRangeException;
import com.allstate.javaassessment.javaassessmentspring.dao.PaymentRepo;
import com.allstate.javaassessment.javaassessmentspring.dto.Payment;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.isA;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PaymentServiceImplMockTest {

    @Mock
    private PaymentRepo paymentRepo ;


    @Test
    public void testRowCount(){
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        when(paymentRepo.count()).thenReturn(3L);
        long count = paymentService.rowcount();
        System.out.println(count);
        assertTrue(count>0);

    }

    @Test
    public void testFindByIdThrowsException() throws OutOfRangeException {
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        assertThrows(OutOfRangeException.class,()->{ Optional<Payment> payment = paymentService.findById(0);});

    }

    @Test
    public void testFindByIdReturnsPayment() throws OutOfRangeException{
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo) ;
        Date date = new Date();
        Payment payment = new Payment(1,date,"cash",200.0,123);
        when(paymentRepo.findById(ArgumentMatchers.anyInt())).thenReturn(Optional.of(payment));
        Optional<Payment> payment1 =paymentService.findById(3);

        assertThat(payment1, isA(Optional.class));
        //assertEquals(1,payment1.);
        }

    @Test
    public void testFindByTypeReturnsException (){
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        assertThrows(OutOfRangeException.class,()->{ List<Payment> payments = paymentService.findByType("");});

    }

    @Test
    public void testFindByType() throws OutOfRangeException {
        PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
        List<Payment> payments = new ArrayList<>();
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "cash", 200.0, 123);
        Payment payment2 = new Payment(2, date, "cash", 250.0, 124);
        Payment payment3 = new Payment(3, date, "cash", 300.0, 125);
        payments.add(payment1);
        payments.add(payment2);
        payments.add(payment3);
        when(paymentRepo.findByType(ArgumentMatchers.anyString())).thenReturn(payments);
        List<Payment> payments1 = paymentService.findByType("cash");
        assertTrue(payments1.size() > 0);
    }
        @Test
        public void testSaveReturnIntValue() throws OutOfRangeException{
            PaymentService paymentService = new PaymentServiceImpl(paymentRepo) ;
            Date date = new Date();
            Payment payment1 = new Payment(1,date,"cash",200.0,123);
            when(paymentRepo.save(payment1)).thenReturn(payment1);
            Payment result = paymentService.save(payment1);
            assertEquals(1,result.getId());

        }
        @Test
        public void testSaveThrowsException(){
            Date date = new Date();
            Payment payment = new Payment(-1,date,"card",100,123);
            PaymentService paymentService = new PaymentServiceImpl(paymentRepo);
            assertThrows(OutOfRangeException.class,()->{ Payment returnPayment = paymentService.save(payment);});

        }
    }




