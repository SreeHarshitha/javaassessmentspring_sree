package com.allstate.javaassessment.javaassessmentspring.services;


import com.allstate.javaassessment.javaassessmentspring.dto.Payment;
import com.allstate.javaassessment.javaassessmentspring.exceptions.OutOfRangeException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.isA;
import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
public class
PaymentServiceImplTest {

    @Autowired
    PaymentService paymentService;

    @Test
    public void testRowCount(){

        long count = paymentService.rowcount();
        assertTrue(count>0);

    }
    @Test
    public void testFindByIdThrowsException() throws OutOfRangeException {

        assertThrows(OutOfRangeException.class,()->{ Optional<Payment> payment = paymentService.findById(0);});

    }

    @Test
    public void testFindByIdReturnsPayment() throws OutOfRangeException{
        Date date = new Date();
        Payment payment = new Payment(1,date,"cash",200.0,123);
        Optional<Payment> payment1 =paymentService.findById(3);
        assertThat(payment1, isA(Optional.class));

    }
    @Test
    public void testFindByTypeReturnsException (){

        assertThrows(OutOfRangeException.class,()->{ List<Payment> payments = paymentService.findByType("");});

    }

    @Test
    public void testFindByType() throws OutOfRangeException {

        List<Payment> payments = new ArrayList<>();
        Date date = new Date();
        Payment payment1 = new Payment(1, date, "cash", 200.0, 123);
        Payment payment2 = new Payment(2, date, "cash", 250.0, 124);
        Payment payment3 = new Payment(3, date, "cash", 300.0, 125);
        payments.add(payment1);
        payments.add(payment2);
        payments.add(payment3);
        List<Payment> payments1 = paymentService.findByType("cash");
        assertTrue(payments1.size() > 0);
    }
    @Test
    public void testSaveReturnIntValue() throws OutOfRangeException{

        Date date = new Date();
        Payment payment1 = new Payment(1,date,"cash",200.0,123);
        Payment result = paymentService.save(payment1);
        assertEquals(1,result.getId());

    }
    @Test
    public void testSaveThrowsException(){
        Date date = new Date();
        Payment payment = new Payment(-1,date,"card",100,123);
        assertThrows(OutOfRangeException.class,()->{ Payment returnPayment = paymentService.save(payment);});

    }
    @Test
    public void testSaveAddsDateIfNotPresent() throws OutOfRangeException {
        Payment payment = new Payment(1,null,"card",100,123);
        Payment result = paymentService.save(payment);
        assertEquals(1,result.getId());
    }
    @Test
     public void testfindAll() {
        List<Payment> paymentList = paymentService.findall();
        assertTrue(paymentList.size()>0);
    }
}
