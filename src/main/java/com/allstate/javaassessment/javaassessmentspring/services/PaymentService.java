package com.allstate.javaassessment.javaassessmentspring.services;

import com.allstate.javaassessment.javaassessmentspring.exceptions.OutOfRangeException;
import com.allstate.javaassessment.javaassessmentspring.dto.Payment;

import java.util.List;
import java.util.Optional;

public interface PaymentService {
    long rowcount();
    Optional<Payment> findById(int id) throws OutOfRangeException;
    List<Payment> findByType(String type) throws OutOfRangeException;
    Payment save(Payment payment) throws OutOfRangeException;
    List<Payment> findall();
}
