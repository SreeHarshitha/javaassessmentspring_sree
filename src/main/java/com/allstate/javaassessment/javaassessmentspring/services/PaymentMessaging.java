package com.allstate.javaassessment.javaassessmentspring.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class PaymentMessaging {
    @Value("${queuename}")
    private String queueName;
    @Value("${topicname}")
    private String topicname;

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendQueueMessage(String Message){
        String messageToSend = Message +" ["+ new Date()+ " ]";
        jmsTemplate.convertAndSend(queueName , messageToSend);
    }
    public void sendTopicMessage(String Message){

        String messageToSend = Message +" ["+ new Date()+ " ]";
        jmsTemplate.setPubSubDomain(true);
        jmsTemplate.convertAndSend(topicname , messageToSend);
    }
}
