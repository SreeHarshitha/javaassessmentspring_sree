package com.allstate.javaassessment.javaassessmentspring.services;

import com.allstate.javaassessment.javaassessmentspring.exceptions.OutOfRangeException;
import com.allstate.javaassessment.javaassessmentspring.dao.PaymentRepo;
import com.allstate.javaassessment.javaassessmentspring.dto.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentRepo paymentRepo;

    public PaymentServiceImpl(PaymentRepo paymentRepo){
        this.paymentRepo = paymentRepo;
    }
    @Override
    public long rowcount() {
        return paymentRepo.count();
    }

    @Override
    public Optional<Payment> findById(int id) throws OutOfRangeException {
        if(id<1){
            throw new OutOfRangeException("Id should be greater than 1");
        }
        return paymentRepo.findById(id);
    }

    @Override
    public List<Payment> findByType(String type) throws OutOfRangeException {
        if(type.equals("")){
            throw new OutOfRangeException("Type cant be null");
        }
        return paymentRepo.findByType(type);


    }

    @Override
    public Payment save(Payment payment) throws OutOfRangeException {
    if(payment.getId()<=0){
        throw new OutOfRangeException("Id should be a positive integer");
    }
    if(payment.getPaymentdate()==null) {
        payment.setPaymentdate(new Date());
    }
        return paymentRepo.save(payment);
    }

    @Override
    public List<Payment> findall(){

        return paymentRepo.findAll();
    }
}
