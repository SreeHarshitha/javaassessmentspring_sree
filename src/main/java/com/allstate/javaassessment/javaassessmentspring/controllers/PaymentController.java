package com.allstate.javaassessment.javaassessmentspring.controllers;

import com.allstate.javaassessment.javaassessmentspring.dto.Payment;
import com.allstate.javaassessment.javaassessmentspring.exceptions.OutOfRangeException;
import com.allstate.javaassessment.javaassessmentspring.services.KafkaPublisher;
import com.allstate.javaassessment.javaassessmentspring.services.PaymentMessaging;
import com.allstate.javaassessment.javaassessmentspring.services.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("/api/payment")
public class PaymentController {

    private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);
    @Autowired
    PaymentService paymentService;
   // @Autowired
   // private JmsTemplate jmsTemplate;
    @Autowired
    KafkaPublisher kafkaPublisher;
    @Autowired
    PaymentMessaging paymentMessaging;
    @GetMapping("status")
    public String getStatus(){
        logger.info(String.format("***status method started***"));
        String message= "Sree Payment Service is up and Running";
        kafkaPublisher.sendMessage("f1" ,message);
        return message;
    }
    @PostMapping("save")
    public void save(@RequestBody Payment payment) throws OutOfRangeException {
       // Customer customer = new Customer(1,"dee",new Address("l1","l2","l3"));
        paymentService.save(payment);
        paymentMessaging.sendTopicMessage(payment.getType()+ "is created");
    }
    @GetMapping("findbyid/{id}")
    public Optional<Payment> findbyid(@PathVariable int id) throws OutOfRangeException {
        // Customer customer = new Customer(1,"dee",new Address("l1","l2","l3"));
        logger.info(String.format("***findbyid method started***"));
        paymentMessaging.sendQueueMessage("A search for"+id+"is done");
        return paymentService.findById(id);

    }
    @GetMapping("findbytype/{type}")
    public List<Payment> findbyType(@PathVariable String type) throws OutOfRangeException {
        return paymentService.findByType(type);
    }
    @GetMapping("getrowcount")
    public long getRowCount(){
        return paymentService.rowcount();
    }


    @GetMapping("find")
    public List<Payment> findall(){
        return paymentService.findall();
    }

}
