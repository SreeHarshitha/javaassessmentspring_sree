package com.allstate.javaassessment.javaassessmentspring.exceptions;

public class OutOfRangeException extends Exception{
    private int severity;

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    public OutOfRangeException(String message) {
        super(message);
    }

    public OutOfRangeException(String message, int severity) {
        super(message);
        this.severity = severity;
    }
}
