package com.allstate.javaassessment.javaassessmentspring.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;




@ControllerAdvice
public class ControllerExceptionHandlet extends ResponseEntityExceptionHandler {
    @ExceptionHandler(OutOfRangeException.class)

    @ResponseStatus(HttpStatus.NOT_FOUND)

    public ResponseEntity<String> handleOutofRange(OutOfRangeException ex, WebRequest request) {

        String errorDetails= ex.getMessage() + "URL" + request.getDescription(false);

        return new ResponseEntity(errorDetails, HttpStatus.NOT_FOUND);

    }


}
