package com.allstate.javaassessment.javaassessmentspring.dao;

import com.allstate.javaassessment.javaassessmentspring.dto.Payment;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PaymentRepo extends MongoRepository<Payment,Integer> {
    List<Payment> findByType(String type);
}
