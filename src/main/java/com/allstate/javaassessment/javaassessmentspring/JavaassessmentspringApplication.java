package com.allstate.javaassessment.javaassessmentspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaassessmentspringApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaassessmentspringApplication.class, args);
    }

}
